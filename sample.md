# FSearch function (Dos)

## Purpose

Searches for a file in a list of directories.

## Declaration

```
function FSearch(Path: PathStr; DirList: String): PathStr;
```

## Remarks

Searches for the file given by *Path* in the list of directories given by
*DirList*. The directories in *DirList* must be separated by semicolons, just
like the directories specified in a PATH command in DOS. The search always
starts with the current directory of the current drive. The returned value is a
concatenation of one of the directory paths and the file name, or an empty
string if the file could not be located.

To search the PATH used by DOS to locate executable files, call
*GetEnv*('PATH') and pass the result to *FSearch* as the *DirList* parameter.

The result of *FSearch* can be passed to *FExpand* to convert it into a fully
qualified name, that is, an uppercase file name that includes both a drive
letter and a root-relative directory path. In addition, you can use *FSplit* to
split the file name into a drive/directory string, a file-name string, and an
extension string.

## See also

FExpand, FSplit, GetEnv

## Example

```
uses Dos;
var S: PathStr;
begin
end.
```


# FSplit procedure (Dos)

## Purpose

Splits a file name into its three components.

## Declaration

```
procedure FSplit(Path: PathStr; var Dir: DirStr; var Name: NameStr;
    var Ext: ExtStr);
```

## Remarks

Splits the file name specified by *Path* into its three components. *Dir* is
set to the drive and directory path with any leading and trailing backslashes,
*Name* is set to the file name, and *Ext* is set to the extension with a
preceding dot. Each of the component strings might possibly be empty, if *Path*
contains no such component.

*FSplit* never adds or removes characters when it splits the file name, and the
concatenation of the resulting *Dir*, *Name*, and *Ext* will always equal to
the specified *Path*.

See page 44 for a list of *File-handling string types*.

## See also

FExpand, File-handling string types

## Example

```
uses Dos;
var
  P: PathStr;
  D: DirStr;
  N: NameStr;
  E: ExtStr;
begin
  FSplit(P, D, N, E);
end.
```


# GetArcCoords procedure (Graphs)

## Purpose

Lets the user inquire about the coordinates of the last *Arc* command.

## Declaration

```
procedure GetArcCoords(var ArcCoords: ArcCoordstype);
```

## Remarks

*GetArcCoords* returns a variable of type *ArcCoordsType*. *GetArcCoords*
returns a variable containing the center point *(X, Y)*, the starting position
*(Xstart, Ystart)*, and the ending position *(Xend, Yend)* of the last *Arc* or
*Ellipse* command. These values are useful if you need to connect a line to the
end of an ellipse.

## Restrictions

Must be in graphics mode.

## See also

Arc, Circle, Ellipse, FillEllipse, PieSlice, PieSliceXY, Sector

## Example

```
uses Graph;
var
  Gd, Gm: Integer;
  ArcCoords: ArcCoordsType;
begin
  Arc(100, 100, 0, 270, 30);
  GetArcCoords(ArcCoords);
end.
```


# GetArgCount function (WinDos)

## Purpose

Returns the number of parameters passed to the program on the command line.

## Declaration

```
function GetArgCount: Integer;
```

## See also

GetArgStr, ParamCount, ParamStr

# sin (Intrinsic)

## Purpose

Computes the `sin(x)` function in radians.

## Declaration

```
real(p) function sin_sp(x)
real(p), intent(in) :: x
end function
```

## Remarks

Computes the `sin(x)` function where `x` is a `real` number given in radians.
The result of `sin(x)` is a `real` number with the same kind as `x`.

The function is a generic function implemented for all `real` kinds supported
by the compiler.

Often the compiler can select from several `sin` implementations, such as:

* Debug mode: high accuracy first, performance second
* Release mode: high performance first, accuracy second

Typically, the Debug mode implementation returns either correctly rounded
answer or the next floating point number for all `x` in the whole `real(p)`
range. Use this version when developing a numerical algorithm to ensure maximum
accuracy. Converge your code to return a correct answer.

The Release mode typically returns `~1e-15` relative accuracy, and the
supported range is smaller, such as `(-1e10, 1e10)`. Use this mode to obtain
maximum performance from your code. The results your code gives can slighly
differ from Debug mode, ensure the difference is acceptable. If it is not, then
continue using the Debug version.

## See also

cos, tan, sinh, cosh, tanh, asin, acos, atan, asinh, acosh, atanh

## Example

```
integer, parameter :: dp = kind(0.d0)
real(dp) :: x, y
x = 3.5_dp
y = sin(x)
print *, y
end.
```
