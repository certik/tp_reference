# Intro

Here is a nice webpage that has Turbo Pascal documentation:

http://turbopascal.org/turbo-pascal-download

in particular the reference guide:

https://turbopascal.org/wp-content/uploads/Turbo_Pascal_Version_7.0_Programmers_Reference_1992.pdf

say page 57 (which is page 68 in the pdf) is a nice example how it looks like.

This repository is trying to recreate that style. Here is the generated latex
output:

https://gitlab.com/certik/tp_reference/-/blob/master/sample.pdf

# Build

```
mamba create -n tp tectonic markdown-it-py
conda activate tp
tectonic sample.tex
```

# Links

Fonts available to download: https://tug.org/FontCatalogue/
